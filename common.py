from googleapiclient import discovery
from google.oauth2 import service_account
from email.mime.text import MIMEText
import base64

import sys

import requests

import settings

_credentials = {"_delegated": {}}
_email_service = None

SCOPES = {
	"directory": ["https://www.googleapis.com/auth/admin.directory.user.readonly"],
	"audit": ["https://www.googleapis.com/auth/drive.metadata.readonly"],
	"lockdown": ["https://www.googleapis.com/auth/drive"],
	"gmail": ["https://www.googleapis.com/auth/gmail.readonly"],
	"sending": ["https://www.googleapis.com/auth/gmail.send"]
}

PUBLIC_PERMISSION_ID = "anyoneWithLink"
DISCOVERABLE_PERMISSION_ID = "anyoneCanFind"

CALENDAR_URL = "https://calendar.google.com/calendar/embed?src={email}&ctz=America%2FChicago"

def print_progress(current_idx, users):
	sys.stdout.write("\r[{}{}]".format(
		'.' * current_idx,
		' ' * (len(users) - current_idx - 1)))

def credentials(category):
	if category in _credentials:
		return _credentials[category]
	elif category in SCOPES:
		_credentials[category] = service_account.Credentials.from_service_account_file(
			settings.SERVICE_ACCOUNT_FILE,
			scopes=SCOPES[category])
		return _credentials[category]
	else:
		raise

def delegated_credentials(email, category):
	if category not in _credentials["_delegated"]:
		_credentials["_delegated"][category] = {}
	if email in _credentials["_delegated"][category]:
		return _credentials["_delegated"][category][email]
	_credentials["_delegated"][category][email] = credentials(category).with_subject(email)
	return _credentials["_delegated"][category][email]

def collect_paginated(engine, field, **kwargs):
	page_size = 1000
	results = engine(**kwargs).execute()
	page_token = results.get("nextPageToken", None)
	items = results.get(field, [])
	while page_token is not None:
		results = engine(pageToken=page_token, **kwargs).execute()
		page_token = results.get("nextPageToken", None)
		items.extend(results.get(field, []))
	return items

def get_domain_users(fields_override=None):
	if fields_override:
		fields = fields_override
	else:
		fields = "nextPageToken, users(primaryEmail, name)"
	directory = discovery.build(
		"admin",
		"directory_v1",
		credentials=delegated_credentials(settings.ADMIN_USERNAME, "directory"))
	users = collect_paginated(
		directory.users().list,
		"users",
		fields=fields,
		domain=settings.DOMAIN)
	return users

def get_publicly_shared_files(email, fields_override=None, query_override=None):
	if fields_override:
		fields = fields_override
	else:
		fields = "nextPageToken, files(id, name, webViewLink, permissionIds, permissions, modifiedTime)"
	if query_override:
		query = query_override
	else:
		query = "'{email}' in owners and (visibility='{vis1}' or visibility='{vis2}')".format(
			email=email,
			vis1=PUBLIC_PERMISSION_ID,
			vis2=DISCOVERABLE_PERMISSION_ID)
	drive = discovery.build(
		"drive",
		"v3",
		credentials=delegated_credentials(email, "audit"))
	try:
		items = collect_paginated(
			drive.files().list,
			"files",
			fields=fields,
			q=query)
		return items
	except:
		# User does not have Google Drive enabled
		return ""

def get_public_role(file):
	public_permission = [p for p in file['permissions'] if p['id'] == 'anyoneWithLink'][0]
	return public_permission['role']

def filter_folders(files):
	return [f for f in files if "folders" in f.get("webViewLink", "")]

def filter_files_unmodified_since(files, cutoff):
	return [f for f in files if f.get("modifiedTime", "") < cutoff]

def is_calendar_private(email):
	r = requests.get(CALENDAR_URL.format(email=email))
	return not r.url.startswith("https://calendar.google.com/calendar/u/0/embed")

def get_forwarders(user_address):
	global_forwarder = False
	gmail = discovery.build(
		"gmail",
		"v1",
		credentials=delegated_credentials(user_address, "gmail"))
	gsettings = gmail.users().settings()
	forwarders = gsettings.forwardingAddresses().list(userId=user_address).execute()
	try:
		forwarding_addresses = [x['forwardingEmail'] for x in forwarders['forwardingAddresses']]
	except KeyError:
		forwarding_addresses = []
	auto_forwarding = gsettings.getAutoForwarding(userId=user_address).execute()
	if auto_forwarding['enabled']:
		global_forwarder = auto_forwarding['emailAddress']
	return [forwarding_addresses, global_forwarder]

def filter_permitted_forwarders(destination_addresses):
	return [
		x
		for x in destination_addresses
		if x.split('@')[-1] not in settings.PERMITTED_FORWARDING_DOMAINS]

def send_email(recipient, subject, body):
	global _email_service
	print("sending email to {}".format(recipient))
	if not _email_service:
		_email_service = discovery.build('gmail', 'v1', credentials=delegated_credentials(settings.ADMIN_USERNAME, "sending"))
	message = create_email(recipient, subject, body)
	email = _email_service.users().messages().send(userId='me', body=message).execute()
	return email

def create_email(to, subject, body):
	message = MIMEText(body, 'html')
	message['to'] = to
	message['subject'] = subject
	return {'raw': base64.urlsafe_b64encode(message.as_bytes()).decode()}