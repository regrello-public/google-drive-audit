# Google Workspace Audit Tools

Identify insecure configurations within Google Workspaces, including publicly shared Google Drive files, public calendars, and external email forwarder configurations.

## Scripts

### audit.py
By default, this script will execute all three audit checks: public Drive files, public calendars, and unexpected email forwarders. Result summaries are printed to stdout, and full results are written to an output directory called `out-[timestamp]`.

Usage:
```
python audit.py
```

##### Public Google Drive Files
Discovers all files in individual Google Drives whose sharing setting is set to "anyone with the link can access". Such configuration is inappropriate for sensitive files, because access is not controlled, the files can be accessed anonymously, and access cannot be audited.

Produces an HTML file in the output directory for each user called `drive_[email].html`, representing the email to be sent to the user, named according to their email address. These are produced using the list of discovered files and the `email_template.html` file. These files have the email subject and destination email address at the top, for easy copy-pasting into Gmail.

Usage:
```
python audit.py drive
```

##### Public Google Calendars
Discovers all users who have set their calendars to be publicly accessible. Such configuration may be inappropriate, since calendars can contain video conferencing passwords, sensitive information, and PII (e.g. interview schedules).

Produces a single output file in the output directory called `public_calendars.txt` which lists the email addresses of all users with a public calendar.

Usage:
```
python audit.py calendar
```

##### External Email Forwarders
Discovers all users who have configured external email addresses as forwarders. Such configuration may be inappropriate as it leaks internal email data externally.

Produces a single output file in the output directory called `email_forwarders.txt` which lists users that have external forwarders configured, as well as the destination addresses. If a global email forwarder is configured, this address is annotatated with "(GLOBAL)".

Usage:
```
python audit.py forwarder
```

### lockdown.py
This automatically removes public sharing of any files last modified more than a month ago that are owned by the specified user.
It retains the share role (e.g. "reader", "editor", etc.) but restricts it to the domain.

Usage:
```
python lockdown.py user@example.com
```

This script stores its output in an output file `out-ld-[email].txt` in a TSV format, in addition to outputting this to the screen as it goes.
It outputs as it goes, which may be useful if the script fails for whatever reason a thousand files into a 2000-file operation.

## Installation

Ensure you have a reasonable version of Python3 (e.g. 3.10.6), pyenv, and pyenv-virtualenv installed.

Set up your virtual environment:
```
$ pyenv virtualenv 3.10.6 gwat
$ pyenv activate gwat
$ pip install -r requirements.txt
```

## Credentials

### Configuration

Copy `settings.example` to `settings.py` and adjust the settings according to your needs:
```
$ cp settings.example settings.py
$ vim settings.py
```

Then set `DOMAIN` to whatever your domain is, and so on.


### Account Setup

The Google Cloud project you use here doesn't make much of a difference, so use whatever is most contextually appropriate in your organization.

Enable the relevant APIs in the Google Cloud project:
* Admin SDK API (to get the list of users)
* Google Drive API (to audit Google Drive files)
* GMail API (to audit email forwarders)

Create a service account in the Google Cloud project:
* Go to the Google Cloud Console [Service Accounts configuration](https://console.cloud.google.com/iam-admin/serviceaccounts)
* Select a relevant project
* Click "Create Service Account"
  * Give it a descriptive name, e.g. `google-drive-auditor`
  * Skip "Grant this service account access to project" and "Grant users access to this service account"
* Once the account is created, click the "Keys" tab
* Click "Add Key", and download the credentials JSON file
* Copy the file into this directory, and ensure the filename matches `SERVICE_ACCOUNT_FILE` in `settings.py`

### Domain-wide Delegation

We need to grant this service account certain scopes that it can use domain-wide.

* Go to the Google Admin Console [Domain-wide Delegation configuration](https://admin.google.com/ac/owl/domainwidedelegation)
* Click "Add new"
* Enter your service account's client ID
* Enter the following scopes:
  * `https://www.googleapis.com/auth/admin.directory.user.readonly`
    * this is used to find all users in your domain, which we will iterate over
  * `https://www.googleapis.com/auth/drive.metadata.readonly`
    * this is used to find publicly-shared files in Google Drive
  * `https://www.googleapis.com/auth/gmail.readonly`
    * this is used to read email forwarder settings
  * `https://www.googleapis.com/auth/gmail.send`
    * this is used to automatically send the audit emails
  * `https://www.googleapis.com/auth/drive`
    * this is only needed for if you intend to use `lockdown.py`
