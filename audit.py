import datetime
import os
import sys

import common
import settings

def audit_forwarders(outdir, users):
	print("Auditing email forwarders")
	for idx, user in enumerate(users):
		common.print_progress(idx, users)
		user_email = user['primaryEmail']
		[forwarders, global_forwarder] = common.get_forwarders(user_email)
		unexpected_forwarders = common.filter_permitted_forwarders(forwarders)
		if unexpected_forwarders:
			with open("{}/email_forwarders.txt".format(outdir), "a") as outfile:
				outfile.write(user_email+"\n")
				for destination_address in unexpected_forwarders:
					line = " - {}".format(destination_address)
					if destination_address == global_forwarder:
						line += ' (GLOBAL)'
					outfile.write(line+"\n")
				outfile.write("\n")
	print("")

def audit_calendar(outdir, users):
	print("Auditing public calendars")
	for idx, user in enumerate(users):
		common.print_progress(idx, users)
		user_email = user['primaryEmail']
		if not common.is_calendar_private(user_email):
			print("    ** calendar public **")
			with open("{}/public_calendars.txt".format(outdir), "a") as outfile:
				outfile.write(user_email+"\n")
	print("")

def audit_drive(outdir, users):
	print("Auditing public Drive files")
	with open('email_template.html') as f:
		drive_email_template = f.read()
	for idx, user in enumerate(users):
		common.print_progress(idx, users)
		user_email = user['primaryEmail']
		user_name = user['name'].get('givenName', user_email)
		public_files = common.get_publicly_shared_files(user_email)
		if public_files:
			result_elem = ""
			for file in public_files:
				result_elem += "<li><a href=\"{link}\">{name}</a></li>\n".format(link=file['webViewLink'], name=file['name'])
			output = drive_email_template.format(
				name=user_name,
				result_elem=result_elem,
				company=settings.COMPANY_NAME,
				docs=settings.DOCUMENTATION_LINK,
				signature=settings.EMAIL_SIGNATURE)
			with open("{}/drive_{}.html".format(outdir, user_email), "w") as outfile:
				outfile.write(user_email + "\n")
				outfile.write(settings.EMAIL_SUBJECT + "\n")
				outfile.write("\n")
				outfile.write(output)
			common.send_email(user_email, settings.EMAIL_SUBJECT, output)
	print("")

if __name__ == "__main__":
	outdir = "out-{}".format(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
	os.mkdir(outdir)

	users = common.get_domain_users()
	
	if len(sys.argv) == 1 or "drive" in sys.argv:
		audit_drive(outdir, users)
	if len(sys.argv) == 1 or "calendar" in sys.argv:
		audit_calendar(outdir, users)
	if len(sys.argv) == 1 or "forwarders" in sys.argv:
		audit_forwarders(outdir, users)
	print("Results located in:\n{}".format(outdir))

